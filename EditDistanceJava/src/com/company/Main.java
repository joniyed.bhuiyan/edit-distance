package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        //List<String> stringList = generateStringList();
        List<String> medicineList = getMedicineList();
//        List<String> medicineList = Arrays.asList("PIVALO", "URSO", "NAPA", "COTRIM", "MIGRANIL", "NEUROLEP", "INFLAGIC", "RISEDON PLUS", "PRAZOLOK", "NEOTACK", "PERKIROL");
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a string: ");
        String userString = input.next();

        Map<String, Integer> mapSearchList = new HashMap<>();

        AtomicInteger threshold = new AtomicInteger(2);

//        double start = System.currentTimeMillis();
//        String keyWord = String.valueOf(userString.charAt(0));
//        List<Demo> collect = medicineList.stream().parallel().map(s -> new Demo(s, editDist(userString, s)))
//                .filter(demo -> {
//                    threshold.set(Math.min(userString.length(), threshold.get()));
//                    return demo.getDis() <= threshold.get();
//                }).sorted(Comparator.comparingInt(Demo::getDis)).collect(Collectors.toList());
//
//
//        List<Demo> collect1 = collect.stream().sorted(new Comparator<Demo>() {
//            @Override
//            public int compare(Demo m1, Demo m2) {
//                String s1 = m1.getValue().toLowerCase();
//                String s2 = m2.getValue().toLowerCase();
//                int v1 = m1.getDis();
//                int v2 = m2.getDis();
//                if (s1.startsWith(keyWord) && s2.startsWith(keyWord)) {
//                    if (v1 == v2) {
//                        return s1.compareTo(s2);
//                    } return 1;
//                } else if (s1.startsWith(keyWord)) {
//                    return -1;
//                } else if (s2.startsWith(keyWord)) {
//                    return 1;
//                } else {
//                    return 0;
//                }
//            }
//        }).collect(Collectors.toList());
//        double end = System.currentTimeMillis();




        final int THRESHOLD = Math.min(userString.length(), 2);
        final int MAX_SUGGESTIONS = 5;
        double start = System.currentTimeMillis();
        List<Demo> collect = medicineList
                .stream()
                .parallel()
                .map(s -> new Demo(s, editDist(userString, s)))
                .filter(demo -> demo.getDis() <= THRESHOLD)
                .sorted(Comparator
                        .comparingInt(Demo::getDis)
                        .thenComparing((o1, o2) -> {
                            int c1 = Math.abs(userString.charAt(0) - o1.getValue().charAt(0));
                            int c2 = Math.abs(userString.charAt(0) - o2.getValue().charAt(0));
                            return c1 - c2;
                        }))
                .limit(MAX_SUGGESTIONS)
                .collect(Collectors.toList());
        double end = System.currentTimeMillis();


        System.out.println("Time need: "+(end-start)+"(mili)\n\n"+collect);


    }

    private static List<String> getMedicineList() {
        List<String> medicineList = new ArrayList<>();
        try {
            File file = new File("/home/zaber/IdeaProjects/EditDistanceJava/product_name.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String name = scanner.nextLine();
                medicineList.add(name.substring(9, name.length() - 2));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return medicineList;
    }

    private static List<String> generateStringList() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 15;
        Random random = new Random();
        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < 100000; i++) {
            String generatedString = "";
            generatedString = random.ints(leftLimit, rightLimit + 1)
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            stringList.add(generatedString);
        }
        return stringList;
    }

    private static int editDist(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        int l1 = s1.length();
        int l2 = s2.length();
        if (s1.length() < s2.length()) {
            l2 = l1;
        }


        int[][] table = new int[l1 + 1][l2 + 1];

        for (int i = 0; i < l1 + 1; i++) {
            for (int j = 0; j < l2 + 1; j++) {
                if (i == 0) {
                    table[i][j] = j;
                } else if (j == 0) {
                    table[i][j] = i;
                } else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    table[i][j] = table[i - 1][j - 1];
                } else {
                    table[i][j] = 1 + Math.min(table[i - 1][j], Math.min(table[i][j - 1], table[i - 1][j - 1]));
                }
            }
        }

        return table[l1][l2];
    }
}
