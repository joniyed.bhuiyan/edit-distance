package com.company;

public class Demo {

    private String value;
    private int dis;

    public Demo(String value, int dis) {
        this.value = value;
        this.dis = dis;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getDis() {
        return dis;
    }

    public void setDis(int dis) {
        this.dis = dis;
    }

    @Override
    public String toString() {
        return value+" - dis=" + dis+"\n";
    }
}
